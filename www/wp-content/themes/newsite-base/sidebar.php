<?php
	// The sidebar exists in a seperate file to aid compatibility with Woocommerce and other plugins
	// Conditional tags can be used - such as is_home() - to show different registered sidebars for different templates
	// You can also add the sidebar into any page template with the get_sidebar() function
?>

	<aside class="sidebar col-sm-4">
		<div class="widgets">
			<?php dynamic_sidebar( 'sidebar' ); ?>
		</div>
	</aside>