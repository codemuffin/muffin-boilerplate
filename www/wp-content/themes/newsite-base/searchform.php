<?php // Search form. Uses Bootstrap class for the submit button ?>

<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<input type="search" class="search-field" placeholder="Search..." value="<?php echo get_search_query() ?>" name="s" />
	<input type="submit" class="search-submit btn btn-primary" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
</form>