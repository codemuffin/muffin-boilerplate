<?php // Comments template - based on old default WP template -- TODO: Write custom version ?>

	<?php if( comments_open() ) { ?>

		<section id="comments">
			<?php if ( post_password_required() ) return; ?>

			<?php if ( have_comments() ) { ?>
				
				<h3 id="comments-title">
				<?php
					printf(
						_n( 'One Response to %2$s', '%1$s Responses to %2$s', get_comments_number() ),
						number_format_i18n( get_comments_number() ), '<em>' . get_the_title() . '</em>'
					);
				?>
				</h3>

				<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
					<nav class="nav">
						<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older Comments' ) ); ?></div>
						<div class="nav-next"><?php next_comments_link( __( 'Newer Comments <span class="meta-nav">&rarr;</span>' ) ); ?></div>
					</nav>
				<?php endif; ?>

				<div class="comment list">
				    <?php wp_list_comments( array( 'style' => 'div' ) ); ?>
				</div>

				<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) { ?>
					<nav class="navigation">
						<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older Comments' ) ); ?></div>
						<div class="nav-next"><?php next_comments_link( __( 'Newer Comments <span class="meta-nav">&rarr;</span>' ) ); ?></div>
					</nav>
				<?php } ?>
				
			<?php }; ?>

			<?php comment_form( array( 'class_submit' => 'submit btn btn-primary' ) ); ?>
		</section>

	<?php } ?>	