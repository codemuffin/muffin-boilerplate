<?php
	// The main index file - copy and paste this code to create page templates
	// The content div uses an ID of content to allow easy CSS overwrites of plugins and other stuff
	get_header();
?>

			<div id="content" class="col-sm-8">
				<?php get_template_part( 'sections/loop' ); ?>
			</div>

			<?php get_sidebar(); ?>

<?php get_footer(); ?>