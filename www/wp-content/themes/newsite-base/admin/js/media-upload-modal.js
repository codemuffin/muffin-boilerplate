/* global wp */

// Uploader modal
jQuery( document ).ready( function( $ )
{
	var frame;

	if ( !jQuery( '.uploader-button' ).length )
	{
		return;
	}

	jQuery( '.uploader-button' ).on( 'click', function( e )
	{
		e.preventDefault();

		var $el = jQuery( e.currentTarget );

		// Reopen a frame if it already exists
		if ( frame )
		{
			frame.open();
			return;
		}

		// Get the data attributes
		var $send_url		= $el.data( 'uploader-send-url' );
		var $send_url_img	= $el.data( 'uploader-send-url-image' );
		var $send_filename	= $el.data( 'uploader-send-filename' );
		var $send_title		= $el.data( 'uploader-send-title' );
		var $send_width		= $el.data( 'uploader-send-width' );
		var $send_height	= $el.data( 'uploader-send-height' );

		var $title 			= $el.data( 'uploader-title' );
		var $button_text 	= $el.data( 'uploader-text' );

		// Create the media frame.
		frame = wp.media.frames.frame = wp.media( {
			title: $title,
			button: {
				text: $button_text,
			},
			library: {
				type: 'image' // only allow images
			},
			multiple: false // only allow selection of a single item
		});

		// When an image is selected
		frame.on( 'select', function()
		{
			// We set multiple to false, so only get one image from the uploader
			var attachment = frame.state().get( 'selection' ).first().toJSON();

			// Send the data to various places
			$( '#'+$send_url ).val( attachment.url );
			$( '#'+$send_url_img ).attr( 'src',attachment.url );
			$( '#'+$send_filename ).text( attachment.filename );
			$( '#'+$send_title ).text( attachment.title );
			$( '#'+$send_width ).text( attachment.width );
			$( '#'+$send_height ).text( attachment.height );

			// Uncomment to see everything in the console
			// console.log(attachment);
		});

		// Open the uploader modal
		frame.open();
	});

});