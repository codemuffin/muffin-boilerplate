<?php

// Get Site Options
	$options = get_option('muffin_options');

// Empty Archive
	if ( is_404() && is_archive() ) : $page_title = 'Nothing Available'; 

// 404 Page
	elseif ( is_404() && !is_archive() ) : $page_title = 'Nothing Found';

// Search Results
	elseif( is_search() ) : $page_title = 'Search Results: '.'<em>' . get_search_query() .'</em>';

// Single Page
	elseif ( is_page() ) : $page_title = get_the_title();

// Custom Post Archives 
	elseif ( is_post_type_archive() ) : $page_title = $wp_query->queried_object->label; // Custom post archive
	elseif ( is_tax() ) : $page_title = $wp_query->queried_object->name; // Custom taxonomy archive

// Blog Archives
	elseif ( is_home() )	: $page_title = $options['blog_title'];
	elseif ( is_category() ): $page_title = 'Category: '.'<em>'. single_cat_title('', false) .'</em>';
	elseif ( is_tag() )		: $page_title = 'Tag: '.'<em>'. single_tag_title('', false) .'</em>';
	elseif ( is_author() ) 	: $page_title = 'Author: '.'<em>'. get_the_author(). '</em>';
	elseif ( is_day() )		: $page_title = 'Daily Archives: '.'<em>'. get_the_date(). '</em>';
	elseif ( is_month() )	: $page_title = 'Monthly Archives: '.'<em>'. get_the_date( 'F Y' ) .'</em>';
	elseif ( is_year() )	: $page_title = 'Yearly Archives: '.'<em>'. get_the_date( 'Y' ) .'</em>';
 
// Defaults to the title of the first post within the main query
	else : $page_title = get_the_title();

// End of the whole IF	
	endif;

// Output the H1 ?>
<h1 class="entry-title page-title"><?php echo $page_title; ?></h1>