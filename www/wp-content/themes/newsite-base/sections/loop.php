<?php
// The loop, which puts everything together, inserting template parts using conditional tags.
// Everything here appears inside #content

// Page titles (H1s)
	get_template_part( 'sections/title' ); 

// Category & Taxonomy descriptions
	if( isset($wp_query->queried_object->description) )
		echo '<div class="lead">'. apply_filters('the_content', $wp_query->queried_object->description) .'</div>';	

// Rewind post data after getting H1s and category info
	rewind_posts();

// Main loop
	if( is_404() ) {
		get_template_part('sections/404'); // 404 or empty archive 
	} else {
		while ( have_posts() ) : the_post();
			if( is_singular() ) {
				get_template_part('sections/page');	 // Single page, post or attachment
			} elseif( get_post_type() ) {
				get_template_part('sections/post'); // Posts within archives (including custom taxonomies)
			}
		endwhile;
	}

// Pagination
	muffin_pagination();

// Restore original Post Data
	wp_reset_postdata();
?>