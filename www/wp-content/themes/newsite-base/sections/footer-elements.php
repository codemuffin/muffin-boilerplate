<?php // The footer element, and its containing widgets ?>

	<footer>
		<?php
		// Array containing our three footer widget areas
		$footer_sidebars = array(
			'footer-top',
			'footer-middle',
			'footer-bottom'
		);

		// We use this variable to make sure at least one widget area is active - a fallback will be used if not
		$active_footer_widget_areas = 0;

		// For each widget area in the array above, check to see if it's active, and if so, show it
		foreach ( $footer_sidebars as $fs ) {

			// IF it's active, show the widget area
			if ( is_active_sidebar( $fs ) ) {

				// Add +1 to the active widgets variable, each time a widget area is shown
				$active_footer_widget_areas ++;
				?>

				<section class="<?php echo $fs; ?>">
					<div class="container">
						<div class="row">
							<div class="widgets">
								<?php dynamic_sidebar( $fs ); ?>
							</div>
						</div>
					</div>
				</section>
			<?php }
		}

		if ( $active_footer_widget_areas === 0 ) {

			// If no footer widget areas are active, show the third, with a small bit of content ?>
			<section class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col-md-6 text-center text-left-md">
							<a href="<?php echo home_url(); ?>">
								<i class="fa fa-home" style="font-size: 130%;"></i>
								<?php bloginfo( 'name' ); ?>
							</a>
						</div>
						<div class="col-md-6 text-center text-right-md">
							&copy; Copyright <?php echo date('Y'); ?> | <?php bloginfo( 'name' ); ?>
						</div>
					</div>
				</div>
			</section>

		<?php } ?>
	</footer>