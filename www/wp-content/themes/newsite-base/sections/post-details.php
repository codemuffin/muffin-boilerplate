<?php // Details for blog posts. Only shows for posts ?>

						<?php if( get_post_type() == 'post' ) { ?>

							<div class="post-meta">
								<div class="entry-date">
									<?php echo 'Posted on '. get_the_date(); ?>
								</div>

								<?php if ( count( get_the_category() ) ) { ?>
									<span class="category-links">
										Categories: <?php echo get_the_category_list( ', ' ) ?>
									</span>
								<?php } ?>

								<?php if( count(get_the_category()) && get_the_tag_list( '', ', ' ) ) {
									// Show a seperator if both categories and tags are available
									echo '<span class="entry-sep">|</span>';
								} ?>

								<?php if( get_the_tag_list( '', ', ' ) ) { ?>
									<span class="tag-links">
										Tags: <?php echo get_the_tag_list( '', ', ' ) ?>
									</span>
								<?php } ?>

							</div>

						<?php } ?>