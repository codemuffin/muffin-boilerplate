<?php // 404 or empty archive page. Shows different content depending on which. ?>

	<article id="post-0">
		<div class="entry-content">
			<?php if(is_archive())  {
				// Empty archive
				echo '<p>There is nothing here at the moment.</p>';
			} else {
				// Missing page or file
				echo '<p>This page or file does not currently exist.</p>';
			} ?>
		</div>
	</article>