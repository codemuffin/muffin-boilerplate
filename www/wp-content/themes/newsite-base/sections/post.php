<?php // Non-singular posts - this includes main blog posts page, search page, any archive page like category/tag/date, and custom post archives/taxonomies ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php $options = get_option( 'muffin_options', 'excerpts' );?>

					<div class="row">
						<?php if( $options['post_excerpts'] === 'excerpts' ) { ?>
							<div class="post-thumbnail col-sm-2">

								<?php // Get the set width and height for the 'thumbnail' size
								$permalink_style  = 'width: '.get_option('thumbnail_size_w').'px; ';
								$permalink_style .= 'height: '.get_option('thumbnail_size_h').'px; '; ?>

								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" style="<?php echo $permalink_style; ?>">
									<?php if ( has_post_thumbnail() ) { ?>
										<?php the_post_thumbnail('thumb', array('class' => 'no-retina')); ?>
										<?php the_post_thumbnail('thumb_retina', array('class' => 'retina')); ?>
									<?php } else { ?>
										<img src="<?php echo $options['featuredimage_url']; ?>" alt="" />
									<?php } ?>
								</a>
							</div>
						<?php } ?>

						<div class="<?php echo ($options['post_excerpts'] === 'excerpts' ? 'col-sm-10' : 'col-sm-12'); ?>">
							<div class="post-excerpt">
								<h2 class="entry-title post-title">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
								</h2>

								<div class="entry-content">
									<?php if( $options['post_excerpts'] === 'fullposts' ) {the_content(); wp_link_pages();} else {the_excerpt();} ?>
									<?php if( !empty($options['edit_link_in_archives']) ) edit_post_link(); ?>
								</div>

								<?php get_template_part( 'sections/post-details' ); ?>
							</div>
						</div>
					</div>

				</article>