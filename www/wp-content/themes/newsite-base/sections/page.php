<?php // The page section, used only for pages ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages(); ?>
						<?php edit_post_link(); ?>
					</div>

					<?php get_template_part( 'sections/post-details' ); ?>
					
					<?php // Support for a 'custom_code' field using Advanced Custom Fields - allows usage of custom JS/CSS, and easy lightboxes
					if( function_exists('get_field') && get_field('custom_code') ) { ?>
						<div class="custom-code">
							<?php the_field('custom_code'); ?>
						</div>
					<?php } ?>

					<?php comments_template(); ?> 

				</article>