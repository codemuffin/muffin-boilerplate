<?php

	// HEADER SECTIONS
	// ---------------

	$options = get_option('muffin_options');
	

	// Headerbar - Runs across the top of the page
	if( is_active_sidebar( 'headerbar' ) ) { ?>
		<section class="headerbar hidden-xs">
			<div class="container">
				<div class="row">
					<?php dynamic_sidebar('headerbar'); ?>
				</div>
			</div>
		</section>
	<?php }
	
	// Main Header - Includes the branding widget area and the navigation ?>
	<header>
		<div class="container">
			<div class="row">		
				<div class="branding col-md-4">
					<?php if( is_active_sidebar( 'branding' ) ) {
						// If the branding widget area is active, show it...
						dynamic_sidebar( 'branding' );
					} else {
						// ...otherwise, show the site name next to a muffin logo (with two versions, for Retina support) ?>
						<div class="text-center text-left-md">
							<a href="<?php echo home_url(); ?>">
								<img class="no-retina" src="<?php echo get_template_directory_uri(); ?>/images/header/logo.png" alt="Logo">
								<img class="retina" src="<?php echo get_template_directory_uri(); ?>/images/header/logox2.png" style="display: none;width: 35px;" alt="Logo">
								<?php echo $options['logo_text']; ?>
							</a>
						</div>
					<?php } ?>
				</div>
		
				<?php $nav_classes = ($options['nav_style'] == 'right' ? 'col-md-8 text-right-md' : 'col-md-12 text-left-md'); ?>

				<nav class="main-menu top-menu <?php echo $nav_classes;?> text-center hidden-xs">
					<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
				</nav>
				
				<div class="mobile-menu-toggle hidden-sm hidden-md hidden-lg">
					<i class="fa fa-bars"></i>
				</div>
				
				<nav class="mobile-menu hidden-sm hidden-md hidden-lg">
					<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
				</nav>
			</div>
		</div>
	</header>
	
	<?php // Homepage Elements - Hero area, CTAs and tagline
	if( is_front_page() ) { ?>
		<section class="hero">
			<?php if( is_active_sidebar( 'hero' ) ) {
				// Show the hero widget, if it is active
				dynamic_sidebar( 'hero' );
			} else {
				// Othwerwise, show the blog info in a jumbotron element ?>
				<div class="jumbotron text-center">
					<h1><em><?php bloginfo( 'name' ); ?></em></h1>
					<h4><em><?php bloginfo( 'description' ); ?></em></h4>
				</div>
			<?php } ?>
		</section>

		<?php if( is_active_sidebar( 'ctas' ) ) { ?>
			<section class="ctas">
				<div class="container">
					<div class="row">
						<?php dynamic_sidebar('ctas'); ?>
					</div>
				</div>
			</section>
		<?php }
		
		if( is_active_sidebar( 'tagline' ) ) { ?>
			<section class="tagline">
				<div class="container">
					<div class="row">
						<?php dynamic_sidebar('tagline'); ?>
					</div>
				</div>
			</section>
		<?php }
	} // endif: is_front_page()
	
?>