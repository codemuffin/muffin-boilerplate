jQuery( document ).ready( function( $ )
{
	// Mobile Menu Toggle
	jQuery( '.mobile-menu-toggle' ).click( function( e )
	{
		$( e.currentTarget ).toggleClass( 'mobile-menu-active' );

		$( '.mobile-menu' ).slideToggle();
	});

	// Mobile Menu Parent/Children Toggle
	$( '.mobile-menu li.menu-item-has-children > a , .mobile-menu li.page_item_has_children > a' ).click( function( e )
	{
		e.preventDefault();

		$( e.currentTarget ).next( 'ul' ).slideToggle();
	});
});