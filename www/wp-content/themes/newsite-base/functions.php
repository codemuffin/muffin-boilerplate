<?php // Theme functions


// Include files from /functions/ (use only one of the functions below)
muffin_include_functions();		// Include files selectively
//muffin_include_functions_all();	// Include all files in /theme/functions/ - USE FOR DEV BUILDS ONLY

function muffin_include_functions() {
	require_once('functions/classes.php');		// Custom classes, such as 'not-home' for internal pages
	require_once('functions/enqueue.php');		// Enqueue the scripts and stylesheets - Google fonts and Bootstrap are found here
	require_once('functions/excerpts.php');		// Excerpt functions, such as length and 'continue reading' text
	require_once('functions/options.php');		// Theme options
	require_once('functions/pagination.php');	// Custom pagination - uses a modern design, instead of the classic 'older/newer posts'
	require_once('functions/theme.php');		// Theme setup files
	require_once('functions/widget-areas.php');	// Widget areas (registered sidebars)
	require_once('functions/widgets.php');		// Custom widgets, including CTA and tagline widgets

	require_once('functions/options-menus.php'); // Menu class options
}

/*
function muffin_include_functions_all() {
	foreach ( glob( dirname( __FILE__ ) . '/functions/*.php' ) as $file )
		include $file;
}
*/

?>