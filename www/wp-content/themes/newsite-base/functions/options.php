<?php // Site Options

// Register actions
add_action( 'admin_menu', 'muffin_options_menu' );
add_action( 'admin_init', 'muffin_options_init' );

// Add the menu item ($page_title, $menu_title, $capability, $menu_slug, $function, $icon_url)
function muffin_options_menu() {
	add_menu_page(
		'Site Options',			// $page_title - <title>
		'Site Options', 		// $menu_title (Menu label)
		'manage_options', 		// $capability (required user capabilities [https://codex.wordpress.org/Roles_and_Capabilities])
		'muffin-options', 		// $menu_slug (URL slug)
		'muffin_options_page', 	// $function (callback function, should contain the page code)
		'dashicons-star-filled'	// $icon_url (dashicon [https://developer.wordpress.org/resource/dashicons])
	);
}

function muffin_options_init() {
	register_setting( 'muffin_options', 'muffin_options' ); // ($option_group, $option_name, $sanitize_callback)

	muffin_options_defaults(); // Set up default options

	add_settings_section( 'muffin_options_general',	'', 'muffin_options_general_cb', 'muffin_options_general'); // ($id, $title, $callback, $page)
	add_settings_section( 'muffin_options_personal','', 'muffin_options_personal_cb','muffin_options_personal');
	add_settings_section( 'muffin_options_posts',	'', 'muffin_options_posts_cb',	 'muffin_options_posts');
	add_settings_section( 'muffin_options_widgets',	'', 'muffin_options_widgets_cb', 'muffin_options_widgets');
	add_settings_section( 'muffin_options_advanced','', 'muffin_options_advanced_cb','muffin_options_advanced');
}

// Display the settings page
function muffin_options_page() { ?>

<?php
	// Useful functions
	function issetor(&$var, $default=false) {
		return isset($var) ? $var : $default;
	}
?>

	<div class="muffin-header">
		<div class="container">
			<h1>Muffin Options</h1>
			<?php settings_errors(); ?>
		</div>
	</div>

	<div class="muffin-nav">
		<nav class="navbar navbar-default">
			<div class="container">
				<ul class="nav navbar-nav">
					<li class="active">
						<a href="#main">
							<i class="fa fa-fw fa-2x fa-star"></i>
							<br>General
						</a>
					</li>
					<li>
						<a href="#personal">
							<i class="fa fa-fw fa-2x fa-user"></i>
							<br>Personal
						</a>
					</li>
					<li>
						<a href="#posts">
							<i class="fa fa-fw fa-2x fa-bookmark"></i>
							<!-- file-alt -->
							<br>Posts
						</a>
					</li>
					<li>
						<a href="#widgets">
							<i class="fa fa-fw fa-2x fa-asterisk"></i>
							<br>Wigets
						</a>
					</li>
					<li>
						<a href="#advanced">
							<i class="fa fa-fw fa-2x fa-code"></i>
							<br>Advanced
						</a>
					</li>
				</ul>
			</div>
		</nav>
	</div>

	<form action='options.php' method='post' class="options-form">
		<div class="container">

		<?php settings_fields( 'muffin_options' ); // Generates the hidden inputs required to save data to this option ?>

		<div class="settings-panel current" id="panel-main"><?php do_settings_sections( 'muffin_options_general' ) ?></div>
		<div class="settings-panel" id="panel-personal"><?php do_settings_sections( 'muffin_options_personal' ) ?></div>
		<div class="settings-panel" id="panel-posts"><?php do_settings_sections( 'muffin_options_posts' ); ?></div>
		<div class="settings-panel" id="panel-widgets"><?php do_settings_sections( 'muffin_options_widgets' ); ?></div>
		<div class="settings-panel" id="panel-advanced"><?php do_settings_sections( 'muffin_options_advanced' ); ?></div>

		<?php submit_button(); ?>

		</div>
	</form>
<?php }


// Default settings
function muffin_options_defaults() {

	$muffin_options = get_option('muffin_options'); // Get the options
	if(empty($muffin_options)) $muffin_options = []; // If there are no options, create an empty array to assit with the functions below

	$muffin_options_default['logo_text'] = get_bloginfo('name');
	$muffin_options_default['post_excerpts'] = 'excerpts';
	$muffin_options_default['featuredimage_url'] = get_template_directory_uri().'/images/backgrounds/placeholder.png';
	$muffin_options_default['post_excerpts'] = 'excerpts';
	$muffin_options_default['remember_tab'] = 'refresh-and-save';
	$muffin_options_default['blog_title'] = 'Latest News';
	$muffin_options_default['nav_style'] = 'right';

	## [?] Create 'muffin_options', using the defaults (the data listed above).
	## [-] Does nothing if 'muffin_options' is set (i.e., exists even with empty data). Basically useless here, we can do this better
	// add_option('muffin_options', $muffin_options_default);

	## [?] Add any new default data, but don't overwrite anything. Use this to add new data while preserving user changes
	$muffin_options = array_merge($muffin_options_default,$muffin_options);
	update_option('muffin_options', $muffin_options);

	## [?] Restore defaults but KEEP all other data. Use to both add new data, and overwrite user changes to default data
	// $muffin_options = array_merge($muffin_options,$muffin_options_default);
	// update_option('muffin_options', $muffin_options);

	## [?] Restore defaults and WIPE all other data. Use to both replace all option data with defaults, and clear all user data, emulating a fresh install
	// update_option('muffin_options', $muffin_options_default);

	## [?] Delete the option entirely. Use to remove all 'muffin_options' data from the database, for resets, testing and cleanup
	// delete_option('muffin_options');
}


// Section: General
function muffin_options_general_cb($args) { ?>

	<?php $options = get_option('muffin_options'); ?>

	<div class="row row-title">
		<div class="col-sm-12 col-title"><h3>Header</h3></div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-name">Logo Text</div>
		<div class="col-sm-7 col-setting"><input type="text" name="muffin_options[logo_text]" value="<?php echo $options['logo_text']; ?>"></div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-name">Navigation Style</div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type='radio' name='muffin_options[nav_style]' <?php checked( $options['nav_style'],'right' ); ?> value='right'>
				Right
			</label> <br />
			<label>
				<input type='radio' name='muffin_options[nav_style]' <?php checked( $options['nav_style'],'below' ); ?> value='below'>
				Below
			</label>
			<p class="description">Show the navigation area to the right of the logo, or below it.</p>
		</div>
	</div>

<?php }

// Section: Personal
function muffin_options_personal_cb($args) { ?>

	<?php $options = get_option('muffin_options'); ?>

	<div class="row row-title">
		<div class="col-sm-12 col-title"><h3>Contact Details</h3></div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-name">Name</div>
		<div class="col-sm-7 col-setting"><input type="text" name="muffin_options[name]" value="<?php echo issetor($options['name']); ?>"></div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-name">Email address</div>
		<div class="col-sm-7 col-setting"><input type="text" name="muffin_options[email]" value="<?php echo issetor($options['email']); ?>"></div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-name">Address</div>
		<div class="col-sm-7 col-setting"><textarea name="muffin_options[address]"><?php echo issetor($options['address']); ?></textarea></div>
	</div>

	<div class="row row-title">
		<div class="col-sm-12 col-title"><h3>Social</h3></div>
	</div>

	<div class="row row-info">
		<div class="col-sm-12 col-info"><p>Set your social profile URLs (web address links) here. They can be used throughout the site.</p></div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-name">Facebook</div>
		<div class="col-sm-7 col-setting"><input type="text" name="muffin_options[facebook]" value="<?php echo issetor($options['facebook']); ?>"></div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-name">Twitter</div>
		<div class="col-sm-7 col-setting"><input type="text" name="muffin_options[twitter]" value="<?php echo issetor($options['twitter']); ?>"></div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-name">Google Plus</div>
		<div class="col-sm-7 col-setting"><input type="text" name="muffin_options[googleplus]" value="<?php echo issetor($options['googleplus']); ?>"></div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-name">YouTube</div>
		<div class="col-sm-7 col-setting"><input type="text" name="muffin_options[youtube]" value="<?php echo issetor($options['youtube']); ?>"></div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-name">LinkedIn</div>
		<div class="col-sm-7 col-setting"><input type="text" name="muffin_options[linkedin]" value="<?php echo issetor($options['linkedin']); ?>"></div>
	</div>

<?php }


// Section: Posts
function muffin_options_posts_cb($args) { ?>

	<?php $options = get_option('muffin_options'); ?>

	<div class="row row-title">
		<div class="col-sm-12 col-title"><h3>Blog Archives</h3></div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-name">Posts Style</div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type='radio' name='muffin_options[post_excerpts]' <?php checked( issetor($options['post_excerpts']),'excerpts' ); ?> value='excerpts'>
				Show Excerpts &amp; Featured Images
			</label> <br />
			<label>
				<input type='radio' name='muffin_options[post_excerpts]' <?php checked( issetor($options['post_excerpts']),'excerpts-nothumbs' ); ?> value='excerpts-nothumbs'>
				Show Excerpts
			</label> <br />
			<label>
				<input type='radio' name='muffin_options[post_excerpts]' <?php checked( issetor($options['post_excerpts']),'fullposts' ); ?> value='fullposts'>
				Show Full Posts
			</label>
		</div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-name">Blog Title</div>
		<div class="col-sm-7 col-setting">
			<input type="text" name="muffin_options[blog_title]" value="<?php echo issetor($options['blog_title']); ?>">
			<p class="description">This title shows above the list of posts on your main blog page. It's normally set to 'Latest News' or simply 'Blog'.</p>
		</div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-name">Edit Button in Archives</div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_options[edit_link_in_archives]" <?php checked( issetor($options['edit_link_in_archives']), 'edits-in-archives' ); ?> value="edits-in-archives">
				Show the 'Edit' button in post archives
			</label>
			<p class="description">By default, the Edit button is only shown on single pages or single posts.</p>
		</div>
	</div>

	<div class="row row-title">
		<div class="col-sm-12 col-title"><h3>Default Featured Image</h3></div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-name">URL</div>
		<div class="col-sm-7 col-setting">
			<input class="widefield" type="text" id="f-img-url" name="muffin_options[featuredimage_url]" value="<?php echo issetor($options['featuredimage_url']); ?>" />
		</div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-name">Title</div>
		<div class="col-sm-7 col-setting"><span id="f-img-title"><?php echo issetor($options['featuredimage_title'], '<span class="note">Nothing set yet...</span>'); ?></span></div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-name">Filename</div>
		<div class="col-sm-7 col-setting"><span id="f-img-filename"><?php echo issetor($options['featuredimage_filename'], '<span class="note">Nothing set yet...</span>'); ?></span></div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-name">Width</div>
		<div class="col-sm-7 col-setting"><span id="f-img-width"><?php echo issetor($options['featuredimage_width'], '<span class="note">Nothing set yet...</span>'); ?></span></div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-name">Height</div>
		<div class="col-sm-7 col-setting"><span id="f-img-height"><?php echo issetor($options['featuredimage_title'], '<span class="note">Nothing set yet...</span>'); ?></span></div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-name">Preview</div>
		<div class="col-sm-7 col-setting">
			<img id="f-img-image" src="<?php echo issetor($options['featuredimage_url']); ?>" style="width: 150px;height: auto;" alt=""/>
		</div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-name">Upload New Image</div>
		<div class="col-sm-7 col-setting">
			<input type="button" class="button uploader-button" value="Replace Image"
			data-uploader-send-url="f-img-url" data-uploader-send-url-image="f-img-image"
			data-uploader-send-filename="f-img-filename" data-uploader-send-title="f-img-title"
			data-uploader-send-width="f-img-width" data-uploader-send-height="f-img-height"
			data-uploader-text="Select Image" data-uploader-text="Choose a Default Featured Image" />
		</div>
	</div>

<?php }


// Section: Widgets
function muffin_options_widgets_cb() { ?>

	<?php $options = get_option('muffin_options'); ?>

	<div class="row row-title">
		<div class="col-sm-12 col-title"><h3>Widget Settings</h3></div>
	</div>

	<div class="row row-setting row-alpha row-omega">
		<div class="col-sm-3 col-name">Placeholder</div>
		<div class="col-sm-7 col-setting">Coming soon...</div>
	</div>

<?php }


// Section: Advanced
function muffin_options_advanced_cb() { ?>

	<?php $options = get_option('muffin_options'); ?>

	<div class="row row-title">
		<div class="col-sm-12 col-title"><h3>Options Page</h3></div>
	</div>

	<div class="row row-setting row-alpha row-omega">
		<div class="col-sm-3 col-name">Remember Last Tab</div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type='radio' name='muffin_options[remember_tab]' <?php checked( $options['remember_tab'],'refresh-and-save' ); ?> value='refresh-and-save'>
				On Save &amp; Refresh
			</label> <br />
			<label>
				<input type='radio' name='muffin_options[remember_tab]' <?php checked( $options['remember_tab'],'save-only' ); ?> value='save-only'>
				On Save Only
			</label> <br />
			<label>
				<input type='radio' name='muffin_options[remember_tab]' <?php checked( $options['remember_tab'],'neither' ); ?> value='neither'>
				Neither</small>
			</label>
			<p class="description">
				This options page can remember which tab was last active.
				When you save or refresh, you'll start on that tab again.
				You can set this to happen either all the time, only after a save, or never.
			</p>
		</div>
	</div>

	<div class="row row-title">
		<div class="col-sm-12 col-title"><h3>Options Data</h3></div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-name">Formatted</div>
		<div class="col-sm-7 col-setting">
		<?php if(issetor($options)) : foreach( $options as $key => $value ) { ?>
			<div class="col-sm-5" style="padding-left: 0;"><strong><?php echo $key; ?>:</strong></div>
			<div class="col-sm-7" style="padding-bottom: 5px;">
				<code>
				<?php
					if(!isset($value)) {echo '<span class="note">NULL - not set</span>';}
					elseif(empty($value)) {echo '<span class="note">string(0) - set, but empty</span>';}
					else {echo $value;};
				?>
				</code>
			</div>
		<?php } else : echo 'No options data!'; endif; ?>
		</div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-name">print_r</div>
		<div class="col-sm-7 col-setting">
			<pre><?php print_r($options); ?></pre>
		</div>
	</div>


<?php }


?>