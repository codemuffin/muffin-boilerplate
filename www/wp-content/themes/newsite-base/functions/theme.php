<?php

/* THEME SETUP
--------------------------------*/

// Run the theme setup (when the 'after_setup_theme' hook is run)
add_action( 'after_setup_theme', 'twentyten_setup' );

if ( ! function_exists( 'twentyten_setup' ) ):
	function twentyten_setup() {
		add_editor_style(); // Enable editor-styles.css
		
		add_theme_support( 'automatic-feed-links' ); // Add RSS feed links to HTMl <head>
		add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) ); // HTML5 support
		add_theme_support( 'post-thumbnails' ); // Enable featured images
		add_theme_support( 'title-tag' );
		add_theme_support( 'woocommerce' ); // Support for Woocommerce
	
		// Register Menus
		register_nav_menus( array(
			'primary' => 'Main Menu',
		) );

		// Add Retina media sizes
		add_image_size( 'thumb_retina',  300, 300, true );
		add_image_size( 'medium_retina', 600, 600, true );
	}
endif;

// Maximum width for uploaded images and oEmbeds (required)
if ( ! isset( $content_width ) ) {$content_width = 750;}

?>