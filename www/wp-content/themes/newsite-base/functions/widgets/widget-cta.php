<?php

add_action( 'widgets_init', 'muffin_widget_cta_register' );

/**
* Muffin widget register: CTA
*/
function muffin_widget_cta_register()
{
	$muffin_widget_options = array(
		'widget_options' => array(
			'id_base'     => 'muffin-cta',
			'name'        => '&#9733; Muffin CTA',
			'classcta'   => 'muffin-cta',
			'description' => 'Muffin CTA widget'
		),
		'fields' => array(
			array(
				'key'      => 'title',
				'label'    => 'Title',
				'helptext' => false
			),
			array(
				'key'      => 'icon',
				'label'    => 'FontAwesome Icon',
				'helptext' => 'Leave off the <code>fa-</code> part, eg. "star" (without quotes). View available icons <a href="https://fontawesome.com/v4.7.0/" target="_blank">here</a>.'
			),
			array(
				'key'      => 'heading',
				'label'    => 'Heading',
				'helptext' => false
			),
			array(
				'key'      => 'tagline',
				'label'    => 'Tagline',
				'helptext' => false
			),
			array(
				'key'      => 'button_text',
				'label'    => 'Button Text',
				'helptext' => 'Not required. Says "view" by default. Will not display if the button URL has not been set'
			),
			array(
				'key'      => 'button_url',
				'label'    => 'Button Link URL',
				'helptext' => false
			),
		),

		'defaults' => array(
			'title'       => '',
			'icon'        => 'star',
			'heading'     => '',
			'tagline'     => '',
			'button_text' => '',
			'button_url'  => '',
		),
		'frontend_callback' => 'muffin_cta_widget_frontend'
	);

	register_widget( new Muffin_Widget( $muffin_widget_options ) );
}

/**
 * Muffin widget frontend: Copyright
 */
function muffin_cta_widget_frontend( $args, $instance )
{ ?>

	<?php if(!empty($instance['icon'])) { ?>
		<p>
			<span class="fa-stack fa-3x">
				<i class="fa fa-circle fa-stack-2x"></i>
				<i class="fa fa-<?php echo $instance['icon']; ?> fa-stack-1x fa-inverse"></i>
			</span>
		</p>
	<?php } ?>

	<?php if ( !empty( $instance[ 'heading' ] ) ) { ?>
		<h2><?php echo $instance[ 'heading' ]; ?></h2>
	<?php } ?>

	<?php if ( !empty( $instance[ 'tagline' ] ) ) { ?>
		<h4><?php echo $instance[ 'tagline' ]; ?></h4>
	<?php } ?>

	<?php if ( !empty( $instance[ 'button_url' ] ) ) { ?>
		<p></p>
		<a class="btn btn-primary" href="<?php echo $instance[ 'button_url' ]; ?>">
			<?php echo ( !empty( $instance[ 'button_text' ] ) ) ? $instance['button_text'] : 'View'; ?>
		</a>
	<?php } ?>

<?php }
