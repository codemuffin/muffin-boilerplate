<?php
add_action( 'widgets_init', 'muffin_widget_jumbotron_register' );

/**
* Muffin widget register: Jumbotron
*/
function muffin_widget_jumbotron_register()
{
	$muffin_widget_options = array(
		'widget_options' => array(
			'id_base'     => 'muffin-jumbotron',
			'name'        => '&#9733; Muffin Jumbotron',
			'classname'   => 'muffin-jumbotron',
			'description' => 'Muffin jumbotron widget, for the Hero widget area'
		),
		'fields' => array(
			array(
				'key'      => 'title',
				'label'    => 'Title',
				'helptext' => 'The title is not displayed on your website. It can be used for your own reference'
			),
			array(
				'key'      => 'heading',
				'label'    => 'Heading',
				'helptext' => false
			),
			array(
				'key'      => 'tagline',
				'label'    => 'Tagline',
				'helptext' => false
			),
			array(
				'key'      => 'button_text',
				'label'    => 'Button Text',
				'helptext' => 'Not required. Says "read more" by default. Will not be shown if the button URL has not been set'
			),
			array(
				'key'      => 'button_url',
				'label'    => 'Button URL',
				'helptext' => false
			)
		),
		'defaults' => array(
			'FIELD_KEY' => ''
		),
		'frontend_callback' => 'muffin_jumbotron_widget_frontend'
	);

	register_widget( new Muffin_Widget( $muffin_widget_options ) );
}

/**
 * Muffin widget frontend: Jumbotron
 */
function muffin_jumbotron_widget_frontend( $args, $instance )
{
	// $title = apply_filters('widget_title', $instance['title']);

	// Add custom classes to this widget
	echo str_replace('class="', 'class="jumbotron text-center ', $args['before_widget']);
	?>

	<br class="hidden-xs">

	<div class="h1">
		<?php echo ( !empty ( $instance[ 'heading' ] ) ) ? $instance[ 'heading' ] : bloginfo( 'name' ); ?>
	</div>

	<h4>
		<?php echo ( !empty( $instance[ 'tagline' ] ) ) ? $instance[ 'tagline' ] : bloginfo( 'description' ); ?>
	</h4>

	<?php if ( !empty( $instance[ 'button_url' ] ) ) { ?>
		<p>
			<br>
			<a class="btn btn-primary btn-lg" href="<?php echo $instance['button_url']; ?>">

				<?php if( !empty( $instance[ 'button_text' ] ) )
				{
					echo $instance[ 'button_text' ];
				}
				else
				{
					echo 'read more';
				} ?>

			</a>
		</p>
	<?php }

	echo $args['after_widget'];
}
