<?php
add_action( 'widgets_init', 'muffin_widget_headerbar_register' );

/**
* Muffin widget register: Headerbar
*/
function muffin_widget_headerbar_register()
{
	$muffin_widget_options = array(
		'widget_options' => array(
			'id_base'     => 'muffin-headerbar',
			'name'        => '&#9733; Muffin Headerbar',
			'classname'   => 'muffin-headerbar',
			'description' => 'Muffin headerbar widget, for your contact details'
		),
		'instance'   => array(
			array(
				'key'      => 'title',
				'label'    => 'Title',
				'helptext' => 'The title is not displayed on your website. It can be used for your own reference'
			),
			array(
				'key'      => 'email',
				'label'    => 'Email address',
				'helptext' => false
			),
			array(
				'key'      => 'phone',
				'label'    => 'Phone number',
				'helptext' => false
			)
		),
		'defaults' => array(
			'title' => '',
			'email' => '',
			'phone' => ''
		),
		'frontend_callback' => 'muffin_headerbar_widget_frontend'
	);

	register_widget( new Muffin_Widget( $muffin_widget_options ) );
}

/**
 * Muffin widget frontend: Headerbar
 */
function muffin_headerbar_widget_frontend( $args, $instance )
{
	$title = apply_filters('widget_title', $instance['title']);

	// Add custom classes to this widget
	echo str_replace('class="', 'class="col-md-12 text-right ', $args[ 'before_widget' ]);
	?>

	<?php if( $instance[ 'email' ] ) { ?>
		<i class="fa fa-envelope-o"></i>
		&nbsp;
		<a href="mailto:<?php echo antispambot($instance['email']); ?>">
			<?php echo $instance['email']; ?>
		</a>
	<?php } ?>

	<?php if( $instance[ 'phone' ] ) { ?>
		<i class="fa fa-envelope-o"></i>
		&nbsp;
		<?php echo $instance['phone']; ?>
	<?php } ?>

	<?php echo $args[ 'before_widget' ];
}
