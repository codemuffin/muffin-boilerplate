<?php

/**
 * Muffin widget builder, a shorthand alternative to extending WP_Widget
 */
class Muffin_Widget extends WP_Widget
{
	// Native Methods
	// ------------------------------------------------------------------------

	/**
	 * Widget options, passed to constructor
	 *
	 * @var array
	 */
	public $config;

	/**
	 * Widget constructor
	 *
	 * @see WP_Widget::__construct()
	 */
	function __construct( $config )
	{
		$this->config = $config;

		parent::__construct(
			$config[ 'widget_options' ][ 'id_base' ],
			$config[ 'widget_options' ][ 'name' ],
			$config[ 'widget_options' ]
		);
	}

	/**
	 * Frontend HTML output
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param  array  $args      Display arguments including before_title, after_title, before_widget, and after_widget
	 * @param  array  $instance  The settings for the particular instance of the widget
	 *
	 * @return void
	 */
	function widget( $args, $instance )
	{
		echo $args[ 'before_widget' ];

		// $callback = $this->config[ 'frontend_callback' ];
		// $callback( $instance );

		$this->config[ 'frontend_callback' ]( $args, $instance );

		echo $args[ 'after_widget' ];
	}

	/**
	 * Render settings form
	 *
	 * @see WP_Widget::form()
	 *
	 * @param   array  $instance  Instance array
	 *
	 * @return  void
	 */
	function form( $instance )
	{
		$defaults = $this->config[ 'defaults' ];
		$instance = wp_parse_args( $instance, $defaults );
		$fields   = $this->config[ 'fields' ];

		foreach( $fields as $field )
		{
			$key = $field[ 'key' ];

			// Rendered values
			$label    = $field[ 'label' ];
			$helptext = $field[ 'helptext' ];
			$id       = $this->get_field_id( $key );
			$name     = $this->get_field_name( $key );
			$val      = ( !empty( $instance[$key] ) ) ? $instance[$key] : "";

			// Render
			?>

			<p>
				<label for="<?php echo $id; ?>">
					<?php echo $label; ?>:
				</label>

				<input class="widefat" id="<?php echo $id; ?>" name="<?php echo $name; ?>" value="<?php echo $val; ?>" type="text" />

				<?php if ( $helptext ) { ?>

					<small><?php echo $helptext; ?></small>

				<?php } ?>
			</p>

		<?php }
	}

	/**
	 * Form update handler
	 *
	 * @see WP_Widget::update()
	 *
	 * @param  array  $new_instance  The new instance of the widget (newly saved settings)
	 * @param  array  $old_instance  The old instance of the widget (old settings)
	 *
	 * @return  array  The updated instance of the widget.
	 */
	function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;

		// Always update the title, even if it's not used
		$instance[ 'title' ] = $new_instance[ 'title' ];

		// Loop through fields
		$fields = $this->config[ 'fields' ];

		foreach ( $fields as $field )
		{
			$key = $field[ 'key' ];

			// Skip title, we've already done it above
			if ( $key !== 'title' )
			{
				$instance[ $key ] = $new_instance[ $key ];
			}
		}

		return $instance;
	}
}
