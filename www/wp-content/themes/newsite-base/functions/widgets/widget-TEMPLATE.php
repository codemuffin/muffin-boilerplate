<?php
/*
Replace:
	WIDGET_ID (lowercase)
	WIDGET_NICENAME (Proper Case)
*/

add_action( 'widgets_init', 'muffin_widget_WIDGET_ID_register' );

/**
* Muffin widget register: WIDGET_NICENAME
*/
function muffin_widget_WIDGET_ID_register()
{
	$muffin_widget_options = array(
		'widget_options' => array(
			'id_base'     => 'muffin-WIDGET_ID',
			'name'        => '&#9733; Muffin WIDGET_NICENAME',
			'classname'   => 'muffin-WIDGET_ID',
			'description' => 'Muffin WIDGET_NICENAME widget [...]'
		),
		'fields' => array(
			array(
				'key'      => 'FIELD_KEY',
				'label'    => 'FIELD_LABEL',
				'helptext' => false
			)
		),
		'defaults' => array(
			'FIELD_KEY' => ''
		),
		'frontend_callback' => 'muffin_WIDGET_ID_widget_frontend'
	);

	register_widget( new Muffin_Widget( $muffin_widget_options ) );
}

/**
 * Muffin widget frontend: WIDGET_NICENAME
 */
function muffin_WIDGET_ID_widget_frontend( $fields )
{ ?>

	<div>...</div>

<?php }
