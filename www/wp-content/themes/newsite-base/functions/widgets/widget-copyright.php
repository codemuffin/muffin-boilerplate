<?php

add_action( 'widgets_init', 'muffin_widget_copyright_register' );

/**
* Muffin widget register: Copyright
*/
function muffin_widget_copyright_register()
{
	$muffin_widget_options = array(
		'widget_options' => array(
			'id_base'     => 'muffin-copyright',
			'name'        => '&#9733; Muffin Copyright',
			'classname'   => 'muffin-copyright',
			'description' => 'Muffin copyright widget, for the footer of your site'
		),
		'fields' => array(
			array(
				'key'      => 'copyright_name',
				'label'    => 'Copyright Name',
				'helptext' => 'Leave this blank to use YOUR site name, set in Settings &gt; General'
			)
		),
		'defaults' => array(
			'copyright_name' => ''
		),
		'frontend_callback' => 'muffin_copyright_widget_frontend'
	);

	register_widget( new Muffin_Widget( $muffin_widget_options ) );
}

/**
 * Muffin widget frontend: Copyright
 */
function muffin_copyright_widget_frontend( $args, $instance )
{ ?>
	<div class="copyright-wrap">
		<span class="copyright">
			&copy; <?php echo date( 'Y' ); ?> <span class="copyright-sep">|</span>

			<a href="<?php echo home_url(); ?>">

				<?php if ( !empty( $instance[ 'copyright_name' ] ) )
				{
					echo $instance[ 'copyright_name' ];
				}
				else
				{
					echo bloginfo( 'name' );
				} ?>

			</a>
		</span>
	</div>
<?php }
