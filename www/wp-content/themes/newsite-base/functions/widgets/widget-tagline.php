<?php
add_action( 'widgets_init', 'muffin_widget_tagline_register' );

/**
* Muffin widget register: Tagline
*/
function muffin_widget_tagline_register()
{
	$muffin_widget_options = array(
		'widget_options' => array(
			'id_base'     => 'muffin-tagline',
			'name'        => '&#9733; Muffin Tagline',
			'classname'   => 'muffin-tagline',
			'description' => 'Muffin tagline widget'
		),
		'fields' => array(
			array(
				'key'      => 'title',
				'label'    => 'Title',
				'helptext' => 'The title is not displayed on your website. It can be used for your own reference'
			),
			array(
				'key'      => 'heading',
				'label'    => 'Heading',
				'helptext' => false
			),
			array(
				'key'      => 'text_line1',
				'label'    => 'Text Line 1',
				'helptext' => false
			),
			array(
				'key'      => 'text_line2',
				'label'    => 'Text Line 2',
				'helptext' => 'Appears below the first line on wide layouts (desktop), but follows the first line to form a full sentance on narrow layouts (mobile)'
			),
			array(
				'key'      => 'button_text',
				'label'    => 'Button Text',
				'helptext' => 'Not required. Says "more details" by default. Will not be shown if the button URL has not been set'
			),
			array(
				'key'      => 'button_url',
				'label'    => 'Button URL',
				'helptext' => false
			)
		),
		'defaults' => array(
			'title'       => '',
			'heading'     => '',
			'text_line1'  => '',
			'text_line2'  => '',
			'button_text' => '',
			'button_url'  => ''
		),
		'frontend_callback' => 'muffin_tagline_widget_frontend'
	);

	register_widget( new Muffin_Widget( $muffin_widget_options ) );
}

/**
 * Muffin widget frontend: Tagline
 */
function muffin_tagline_widget_frontend( $args, $instance )
{
	$title = apply_filters( 'widget_title', $instance[ 'title' ] );

	// Add a 12-column class to the widget classes
	echo str_replace( 'class="', 'class="col-sm-12 ', $args[ 'before_widget' ] );

	// Uncomment to show widget title
	// if( $title ) { echo $before_title.$title.$after_title; }
	?>

	<div class="row">

		<div class="<?php if( $instance[ 'button_url' ] ) {echo 'col-sm-8';} else {echo 'col-sm-12';} ?> text-center text-left-sm">
			<div>
				<h2><?php if( $instance[ 'heading' ] ) echo $instance[ 'heading' ]; ?></h2>
				<h4>
					<?php if( $instance[ 'text_line1' ] ) echo $instance[ 'text_line1' ]; ?>
					<?php if( $instance[ 'text_line2' ] ) echo '<br class="visible-lg-inline">'. $instance[ 'text_line2' ]; ?>
				</h4>
			</div>
		</div>

		<?php if ( $instance[ 'button_url' ] ) { ?>
			<div class="col-sm-4 text-center">
				<div>
					<br />

					<a class="btn btn-default" href="<?php echo $instance[ 'button_url' ]; ?>">

						<?php if ( $instance[ 'button_text' ] )
						{
							echo $instance[ 'button_text' ];
						}
						else
						{
							echo 'more details';
						} ?>

					</a>
				</div>
			</div>
		<?php } ?>

	</div>

	<?php echo $args[ 'after_widget' ];
}
