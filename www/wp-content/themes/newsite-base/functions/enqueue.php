<?php

// Enqueue Actions
// ============================================================================

add_action( 'wp_enqueue_scripts',    'muffin_enqueue_styles' );  // Frontend scripts
add_action( 'wp_enqueue_scripts',    'muffin_enqueue_scripts' ); // Frontend styles
add_action( 'admin_enqueue_scripts', 'muffin_enqueue_admin' );   // Admin (custom options pages)


// Enqueue Callbacks
// ============================================================================

/**
 * Enqueue stylesheets
 *
 * @return  void
 */
function muffin_enqueue_styles()
{
	// Vendor
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap.min.css', '', '3.0.3' );

	// Custom
	wp_enqueue_style( 'muffin-basic',  get_template_directory_uri() . '/assets/css/basic.css' );
	wp_enqueue_style( 'muffin-custom', get_template_directory_uri() . '/assets/css/custom.css' );

	// No longer needed, moved all CSS to custom.css
	// wp_enqueue_style( 'style',  get_stylesheet_uri(), array( 'bootstrap' ) );

	// Fonts
	wp_enqueue_style( 'googlefonts', 'https://fonts.googleapis.com/css?family=Open+Sans|Bree+Serif|Pacifico' );
	wp_enqueue_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' );
}

/**
 * Enqueue scripts
 *
 * @return  void
 */
function muffin_enqueue_scripts()
{
	// Vendor
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap/js/bootstrap.min.js', array( 'jquery' ), '3.3.4' );

	// Custom
	wp_enqueue_script( 'muffin-js', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ) );
}

/**
 * Enqueue admin styles & scripts
 *
 * @return  void
 */
function muffin_enqueue_admin( $hook )
{
	// Only load on theme options pages
	if ( $hook != 'toplevel_page_muffin-options' && $hook != 'toplevel_page_muffin-menus' )
	{
		return;
	}

	// Fonts
	wp_enqueue_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' );

	// Vendor
	wp_enqueue_style(  'admin-bootstrap', get_template_directory_uri() . '/admin/bootstrap/css/bootstrap.min.css', '', '3.0.3' );
	wp_enqueue_script( 'admin-bootstrap', get_template_directory_uri() . '/admin/bootstrap/js/bootstrap.min.js', array( 'jquery' ), '3.3.4' );
	wp_enqueue_script( 'js-cookie',       get_template_directory_uri() . '/admin/js/js.cookie.js', array( 'jquery' ) );

	// Custom
	wp_enqueue_style(  'admin-style',          get_template_directory_uri() . '/admin/css/admin.css' );
	wp_enqueue_script( 'media-upload-modal',   get_template_directory_uri() . '/admin/js/media-upload-modal.js', array( 'jquery' ) );
	wp_enqueue_script( 'muffin-settings-page', get_template_directory_uri() . '/admin/js/settings-page.js', array( 'jquery' ) );

	wp_enqueue_media();
}
