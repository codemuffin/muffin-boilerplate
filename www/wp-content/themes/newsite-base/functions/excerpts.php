<?php

/* EXCERPTS
---------------------------------------*/

// Post excerpt length
function custom_excerpt_length( $length ) {return 40;}
add_filter( 'custom_length', 'custom_excerpt_length' );

// Post excerpt post-text (post link and 'read more' text)
function custom_continue_reading_link() {
	return '... <a class="readmore-link" href="'. get_permalink() . '"><span class="readmore-text">read more...</span></a>';
}

// Replaces "[...]" (appended to automatically generated excerpts) with custom_continue_reading_link().
function custom_auto_excerpt_more( $more ) {
	return custom_continue_reading_link();
}
add_filter( 'excerpt_more', 'custom_auto_excerpt_more' );


// Adds custom_continue_reading_link (post link and 'read more' text) to custom post excerpts
function custom_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= custom_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'custom_custom_excerpt_more' );


?>