<?php

/* CUSTOM PAGINATION
---------------------------------------*/
function muffin_pagination() {
	global $wp_query;

	// Only paginate if there's a query var for 'paged'
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	if( !empty($paged) ) {
		$big_number = 999999;			
		echo '<div class="pagination">';
		echo paginate_links( array(
			'base' => str_replace( $big_number, '%#%', esc_url( get_pagenum_link( $big_number ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages
		) );
		echo '</div>';
	
	}
}
?>