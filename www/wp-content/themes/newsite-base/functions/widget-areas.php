<?php

/* WIDGET AREAS
---------------------------------------*/

function custom_widgets_init() {

	// Main Sidebar
	register_sidebar( array(
		'name' => 'Sidebar',
		'id' => 'sidebar',
		'description' => 'The main sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Header Bar (top page strip)
	register_sidebar( array(
		'name' => 'Header Bar',
		'id' => 'headerbar',
		'description' => 'Runs across the top of the site',
		'before_widget' => '<div id="%1$s" class="widget %2$s top-menu">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Header
	register_sidebar( array(
		'name' => 'Branding',
		'id' => 'branding',
		'description' => 'A place for your logo',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	// Hero (slider, jumbotron, large image, etc.)
	register_sidebar( array(
		'name' => 'Hero',
		'id' => 'hero',
		'description' => 'Hero widget area. Usually used with a slider, jumbotron, or large image',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	// CTAs
	register_sidebar( array(
		'name' => 'CTAs',
		'id' => 'ctas',
		'description' => 'CTA widget area. Add 3 widgets to make them appear in 3 columns',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	// Tagline
	register_sidebar( array(
		'name' => 'Tagline',
		'id' => 'tagline',
		'description' => 'Tagline widget area',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Footer 1
	register_sidebar( array(
		'name' => 'Footer - Top',
		'id' => 'footer-top',
		'description' => 'The top footer. Use up to 3 widgets.',
		'before_widget' => '<div id="%1$s" class="widget %2$s col-sm-4">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Footer 2
	register_sidebar( array(
		'name' => 'Footer - Middle',
		'id' => 'footer-middle',
		'description' => 'The middle footer - perfect for your sitemap and contact details. Use 4 widgets.',
		'before_widget' => '<div id="%1$s" class="widget %2$s col-sm-3">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Footer 3
	register_sidebar( array(
		'name' => 'Footer - Bottom',
		'id' => 'footer-bottom',
		'description' => 'The lowest footer - good for copyright information, developer links, and legal copy. Use 2 widgets.',
		'before_widget' => '<div id="%1$s" class="widget %2$s col-sm-6">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

}

// Register sidebars
add_action( 'widgets_init', 'custom_widgets_init' );
?>