<?php

// Register
// ============================================================================

// Builder class (required if using any widgets below)
require_once( 'widgets/widget-BUILDER.php' );

// Widget instances (with auto registers)
require_once( 'widgets/widget-copyright.php' );
require_once( 'widgets/widget-cta.php' );
require_once( 'widgets/widget-headerbar.php' );
require_once( 'widgets/widget-jumbotron.php' );
require_once( 'widgets/widget-tagline.php' );
