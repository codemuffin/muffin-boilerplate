<?php


add_action( 'admin_menu', 'muffin_menus_add_admin_menu' );
add_action( 'admin_init', 'muffin_menus_options_init' );


function muffin_menus_add_admin_menu() {
	add_menu_page(
		'Edit Your Menu Class Options', // Page <title>
		'Menu Classes', // Menu item name
		'manage_options', // Required user capabilities
		'muffin-menus', // Slug (URL part)
		'muffin_menu_form', // Callback function (displays the page)
		'dashicons-menu' // Dashicon
	);
}

/*
current-menu-ancestor
current-menu-item
current-menu-parent
current_page_ancestor
current_page_item
current_page_parent
menu-item
menu-item-home
menu-item-object-category
menu-item-object-page
menu-item-object-tag
menu-item-type-post_type
menu-item-type-taxonomy
page_item
*/


function muffin_menus_options_init() {
	register_setting( 'muffin_menus', 'muffin_menus' );

	muffin_menus_defaults();

	add_settings_section('muffin_menus_general', '', 'muffin_menus_general_cb',  'muffin_menus_general');
	add_settings_section('muffin_menus_old',     '', 'muffin_menus_old_cb',      'muffin_menus_old');
	add_settings_section('muffin_menus_replace', '', 'muffin_menus_replace_cb',  'muffin_menus_replace');
	add_settings_section('muffin_menus_info',    '', 'muffin_menus_info_cb',     'muffin_menus_info');
	add_settings_section('muffin_menus_data',    '', 'muffin_menus_data_cb',     'muffin_menus_data');

}


function muffin_menus_defaults() {

	$options = get_option('muffin_menus');

	$options_default['menu_enable_active_class'] 		= 'enabled';
	$options_default['menu_enable_parent_class'] 		= 'enabled';
	$options_default['menu_enable_depth_classes']		= 'enabled';
	$options_default['menu_enable_submenu_class'] 		= 'disabled';

	$options_default['menu_active_class'] 				= 'menu-active';
	$options_default['menu_parent_class']		 		= 'menu-parent';
	$options_default['menu_depth_class_top'] 			= 'menu-top';
	$options_default['menu_depth_class_below'] 			= 'sub-item';
	$options_default['menu_submenu_class'] 				= 'sub-menu';

	$options_default['menu_enable_remove_ids']			= 'disabled';
	$options_default['menu_enable_remove_classes']		= 'disabled';

	$options_default['preserve_page_item'] 				= 'disabled';
	$options_default['preserve_page_item_has_children'] = 'disabled';
	$options_default['preserve_current_page_item'] 		= 'disabled';
	$options_default['preserve_current_page_parent'] 	= 'disabled';
	$options_default['preserve_current_page_ancestor'] 	= 'disabled';
	$options_default['preserve_menu_item'] 				= 'disabled';
	$options_default['preserve_menu_item_has_children']	= 'disabled';
	$options_default['preserve_current_menu_item'] 		= 'disabled';
	$options_default['preserve_current_menu_parent'] 	= 'disabled';
	$options_default['preserve_current_menu_ancestor'] 	= 'disabled';
	$options_default['preserve_menu_active'] 			= 'enabled';
	$options_default['preserve_menu_parent'] 			= 'enabled';
	$options_default['preserve_depth'] 					= 'enabled';
	$options_default['preserve_submenu'] 				= 'enabled';

	$options_default['menu_update_wp_page_menu'] 		= 'enabled';
	$options_default['menu_fallback_menus_only'] 		= 'enabled';
	$options_default['menu_preserve_old_classes'] 		= 'enabled';
	$options_default['menu_preserve_old_menu_class'] 	= 'disabled';

	## [?] Create 'muffin_menus', using the defaults (the data listed above)
	## [-] Does nothing if 'muffin_menus' is set (i.e., exists even with empty data). Good for creating the option in the first place
	//add_option('muffin_menus', $options_default);

	## [?] Add any new default data, but don't overwrite anything. Use this to add new data while preserving user changes
	//$options = array_merge($options_default,$options);
	//update_option('muffin_menus', $options);

	## [?] Restore defaults but KEEP all other data. Use to both add new data, and overwrite user changes to default data
	// $options = array_merge($options, $options_default);
	// update_option('muffin_menus', $options);

	## [?] Restore defaults and WIPE all other data. Use to both replace all option data with defaults, and clear all user data, emulating a fresh install
	 update_option('muffin_menus', $options_default);

	## [?] Delete the option entirely. Use to remove all 'muffin_menus' data from the database, for resets, testing and cleanup
	// delete_option('muffin_menus');

}

function muffin_menu_form() { ?>

<?php
	// Useful functions
	function issetor(&$var, $default=false) {
		return isset($var) ? $var : $default;
	}
?>

	<div class="muffin-header">
		<div class="container">
			<h1>Muffin Menu Options</h1>
			<?php settings_errors(); ?>
		</div>
	</div>

	<div class="muffin-nav">
		<nav class="navbar navbar-default">
			<div class="container">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#main"><i class="fa fa-fw fa-2x fa-plus"></i><br />Add</a></li>
					<li><a href="#replace"><i class="fa fa-fw fa-2x fa-minus"></i><br />Remove</a></li>
					<li><a href="#oldmenus"><i class="fa fa-fw fa-2x fa-bolt"></i><br />Update</a></li>
					<li><a href="#info"><i class="fa fa-fw fa-2x fa-info"></i><br />Info</a></li>
					<li><a href="#data"><i class="fa fa-fw fa-2x fa-database"></i><br />Data</a></li>
				</ul>
			</div>
		</nav>
	</div>

	<form action='options.php' method='post' class="options-form">
		<div class="container">

		<?php settings_fields( 'muffin_menus' ); ?>

		<div class="settings-panel current" id="panel-main"><?php do_settings_sections( 'muffin_menus_general' ) ?></div>
		<div class="settings-panel" id="panel-oldmenus"><?php do_settings_sections( 'muffin_menus_old' ); ?></div>
		<div class="settings-panel" id="panel-replace"><?php do_settings_sections( 'muffin_menus_replace' ); ?></div>
		<div class="settings-panel" id="panel-data"><?php do_settings_sections( 'muffin_menus_data' ); ?></div>
		<div class="settings-panel" id="panel-info"><?php do_settings_sections( 'muffin_menus_info' ); ?></div>

		<?php submit_button(); ?>

		</div>
	</form>

<?php }


function muffin_menus_general_cb() { $options = get_option('muffin_menus'); ?>

	<div class="row row-title">
		<div class="col-sm-10 col-title"><h3>Add Custom Classes</h3></div>
	</div>

	<div class="row row-info">
		<div class="col-sm-10 col-info"><p>Add your own custom menu classes. Applies to all menu types, including menu widgets.</p></div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-name">Custom Menu Classes</div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_active_class]" <?php checked( issetor($options['menu_enable_active_class']), 'enabled' ); ?> value="enabled">
				Enable Class: Active Item
			</label>
			<p class="description">
				Applies a single class to all active menu items - so your CSS can target current menu items, ancestors and parents with a single selector.
			</p>
		</div>
	</div>

	<div class="row row-setting row">
		<div class="col-sm-3 col-name"></div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_parent_class]" <?php checked( issetor($options['menu_enable_parent_class']), 'enabled' ); ?> value="enabled">
				Enable Class: Parent Item
			</label>
			<p class="description">
				Much more elegant than 'page_item_has_children' and 'menu-item-has-children'.
			</p>
		</div>
	</div>

	<div class="row row-setting row">
		<div class="col-sm-3 col-name"></div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_depth_classes]" <?php checked( issetor($options['menu_enable_depth_classes']), 'enabled' ); ?> value="enabled">
				Enable Depth Classes
			</label>
			<p class="description">
				Give a class of <code>menu-top</code> to all top-level menu items (regardless of if they are parents),
				and a class of <code>sub-item</code> to all menu items within sub-menus. Also, add depth-* classes.
				Mainly this allows you to prevent inherited styles with a single class (instead of excessive selectors).
			</p>
		</div>
	</div>

	<div class="row row-setting row">
		<div class="col-sm-3 col-name"></div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_submenu_class]" <?php checked( issetor($options['menu_enable_submenu_class']), 'enabled' ); ?> value="enabled">
				Customise Sub-Menu Class
			</label>
			<p class="description">
				Change the class of 'sub-menu', which is applied to children UL elements.
			</p>
		</div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-name">Set Custom Classes</div>
		<div class="col-sm-7 col-setting">
			<label>
				<div class="pretext">Active:</div>
				<input type="text" id="f-img-url" name="muffin_menus[menu_active_class]" value="<?php echo issetor($options['menu_active_class']); ?>" />
				&nbsp; <span class="note">Default: <code>menu-active</code></span>
			</label> <br />
			<label>
				<div class="pretext">Parent:</div>
				<input type="text" name="muffin_menus[menu_parent_class]" value="<?php echo issetor($options['menu_parent_class']); ?>" />
				&nbsp; <span class="note">Default: <code>menu-parent</code></span>
			</label> <br />
			<label>
				<div class="pretext">Top Level:</div>
				<input type="text" name="muffin_menus[menu_depth_class_top]" value="<?php echo issetor($options['menu_depth_class_top']); ?>" />
				&nbsp; <span class="note">Default: <code>menu-top</code></span>
			</label> <br />
			<label>
				<div class="pretext">Child Level:</div>
				<input type="text" name="muffin_menus[menu_depth_class_below]" value="<?php echo issetor($options['menu_depth_class_below']); ?>" />
				&nbsp; <span class="note">Default: <code>sub-item</code></span>
			</label> <br />
			<label>
				<div class="pretext">Sub-Menu:</div>
				<input type="text" name="muffin_menus[menu_submenu_class]" value="<?php echo issetor($options['menu_submenu_class']); ?>" />
				&nbsp; <span class="note">Default: <code>sub-menu</code></span>
			</label>
		</div>
	</div>

<?php }


function muffin_menus_old_cb() { $options = get_option('muffin_menus'); ?>

	<div class="row row-title">
		<div class="col-sm-10 col-title"><h3>Old Menus <small>(wp_page_menu)</small></h3></div>
	</div>

	<div class="row row-info">
		<div class="col-sm-10 col-info">
			<p>
			You can insert a custom menu into your template with <a href="https://codex.wordpress.org/Function_Reference/wp_nav_menu" target="_blank">wp_nav_menu</a>.
			However, if no menu is set in the admin, then by default a menu will be generated using <a href="https://codex.wordpress.org/Function_Reference/wp_page_menu" target="_blank">wp_page_menu</a>.
			This function generates menus without the modern classes (like <code>menu-item</code>), but with lots of older ones (like <code>page_item</code>).
			You can use these settings to make wp_page_menu use the modern classes (either instead, or as well).
			</p>
		</div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-name">Add Classes</div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_update_wp_page_menu]" <?php checked( issetor($options['menu_update_wp_page_menu']), 'enabled' ); ?> value="enabled">
				Add modern classes to old menus
			</label>
			<p class="description">
				If checked, the classes WordPress uses for menus created with <code>wp_nav_menu</code> will be applied to <code>wp_page_menu</code> menus too.
				This means your styles will be preserved if the menu becomes unset.
			</p>
		</div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-name">Fallback Menus Only</div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_fallback_menus_only]" <?php checked( issetor($options['menu_fallback_menus_only']), 'enabled' ); ?> value="enabled">
				Only apply settings to fallback menus
			</label>
			<p class="description">
				These old menus are only really a pain when they appear as fallbacks for custom menus. Changing classes to them in other cases could break
				plugins, especially older ones. If this is checked, all of these settings will only apply to fallback menus.
				Note that this checks for a setting of 'theme_location' => 'menu-location' so - this won't work if you're just using 'menu' => 'xxx' on its own.
			</p>
		</div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-name">Menu Classes</div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_preserve_old_classes]" <?php checked( issetor($options['menu_preserve_old_classes']), 'enabled' ); ?> value="enabled">
				Keep the old menu item classes
			</label>
			<p class="description">
				The old menu classes will be removed from old menus, unless you check this box.
				If you're updating your menu classes, these old classes probably won't be needed, but you may have a use or need for them.
			</p>
		</div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-name">Wrapper Class</div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_preserve_old_menu_class]" <?php checked( issetor($options['menu_preserve_old_menu_class']), 'enabled' ); ?> value="enabled">
				Keep a class of 'menu' on the wrapping DIV
			</label>
			<p class="description">
				Old menus use the class <code>menu</code> on the containing DIV, while modern menus use the class on a UL.
				Check this box to leave the .menu class on the DIV.
				It will still be added to the UL though, as part of the menu modernisation, so only check this if you need DIV class preserved for some reason.
			</p>
		</div>
	</div>

<?php }


function muffin_menus_replace_cb() { $options = get_option('muffin_menus'); ?>

	<div class="row row-title">
		<div class="col-sm-10 col-title"><h3>Remove Inbuilt IDs &amp; Classes</h3></div>
	</div>

	<div class="row row-info">
		<div class="col-sm-10 col-info"><p>Use these settings to clean up your WP menus!</p></div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-name">Remove All IDs</div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_remove_ids]" <?php checked( issetor($options['menu_enable_remove_ids']), 'enabled' ); ?> value="enabled">
				Remove All IDs
			</label>
		</div>
	</div>

	<div class="row row-setting">
		<div class="col-sm-3 col-name">Remove All Built-in Classes</div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[menu_enable_remove_classes]" <?php checked( issetor($options['menu_enable_remove_classes']), 'enabled' ); ?> value="enabled">
				Remove all WordPress menu classes
				<p class="description">
					Allows you to remove ALL classes from all WordPress menu items. You can still keep useful classes with the 'keep classes' options below.
					Note that this won't apply to non-fallback uses of wp_page_menu, if you have the 'Apply to Fallback Menus Only' setting checked.
				</p>
			</label>
		</div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-name">Keep Menu Classes</div>
		<div class="col-sm-7 col-setting">
			<label>
				<input type="checkbox" name="muffin_menus[preserve_page_item]" <?php checked( issetor($options['preserve_page_item']), 'enabled' ); ?> value="enabled">
				Keep the wp_page_menu class <code>page_item</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_page_item_has_children]" <?php checked( issetor($options['preserve_page_item_has_children']), 'enabled' ); ?> value="enabled">
				Keep the wp_page_menu class <code>page_item_has_children</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_current_page_item]" <?php checked( issetor($options['preserve_current_page_item']), 'enabled' ); ?> value="enabled">
				Keep the wp_page_menu class <code>current_page_item</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_current_page_parent]" <?php checked( issetor($options['preserve_current_page_parent']), 'enabled' ); ?> value="enabled">
				Keep the wp_page_menu class <code>current_page_parent</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_current_page_ancestor]" <?php checked( issetor($options['preserve_current_page_ancestor']), 'enabled' ); ?> value="enabled">
				Keep the wp_page_menu class <code>current_page_ancestor</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_menu_item]" <?php checked( issetor($options['preserve_menu_item']), 'enabled' ); ?> value="enabled">
				Keep the wp_nav_menu class <code>menu-item</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_menu_item_has_children]" <?php checked( issetor($options['preserve_menu_item_has_children']), 'enabled' ); ?> value="enabled">
				Keep the wp_nav_menu class <code>menu-item-has-children</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_current_menu_item]" <?php checked( issetor($options['preserve_current_menu_item']), 'enabled' ); ?> value="enabled">
				Keep the wp_nav_menu class <code>current-menu-item</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_current_menu_parent]" <?php checked( issetor($options['preserve_current_menu_parent']), 'enabled' ); ?> value="enabled">
				Keep the wp_nav_menu class <code>current-menu-parent</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_current_menu_ancestor]" <?php checked( issetor($options['preserve_current_menu_ancestor']), 'enabled' ); ?> value="enabled">
				Keep the wp_nav_menu class <code>current-menu-ancestor</code>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_menu_active]" <?php checked( issetor($options['preserve_menu_active']), 'enabled' ); ?> value="enabled">
				Keep the custom class <code>menu-active</code>
				<span class="note">(currently set to: <code><?php echo $options['menu_active_class']; ?></code>)</span>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_menu_parent]" <?php checked( issetor($options['preserve_menu_parent']), 'enabled' ); ?> value="enabled">
				Keep the custom class <code>menu-parent</code>
				<span class="note">(currently set to: <code><?php echo $options['menu_parent_class']; ?></code>)</span>
			</label><br />
			<label>
				<input type="checkbox" name="muffin_menus[preserve_depth]" <?php checked( issetor($options['preserve_depth']), 'enabled' ); ?> value="enabled">
				Keep the custom classes <code>menu-top</code> &amp; <code>sub-item</code>
				<span class="note">(currently set to: <code><?php echo $options['menu_depth_class_top']; ?></code> &amp; <code><?php echo $options['menu_depth_class_below']; ?></code>)</span>
			</label>
		</div>
	</div>


<?php }


function muffin_menus_info_cb() { $options = get_option('muffin_menus'); ?>

	<div class="row row-title">
		<div class="col-sm-10 col-title"><h3>Information</h3></div>
	</div>

	<div class="row row-setting row-alpha row-omega">
		<div class="col-sm-3 col-name">Workarounds</div>
		<div class="col-sm-7 col-setting">
			<p class="description">
			There are arguments that you can pass to <code>wp_nav_menu</code> to disable the use of <code>wp_page_menu</code>,
			but if it is disabled, no menu will show. You can work around this too, but using these settings just saves you from all the hassle.
			</p>
		</div>
	</div>

<?php }


function muffin_menus_data_cb() { $options = get_option('muffin_menus'); ?>

	<div class="row row-title">
		<div class="col-sm-10 col-title"><h3>Options Data</h3></div>
	</div>

	<div class="row row-info">
		<div class="col-sm-10 col-info"><p>This section lets you see al your saved data at a glance.</p></div>
	</div>

	<div class="row row-setting row-alpha">
		<div class="col-sm-3 col-name">Formatted</div>
		<div class="col-sm-7 col-setting">
		<?php if(issetor($options)) : foreach( $options as $key => $value ) { ?>
			<?php echo $key; ?>:
			<code>
			<?php
				if(!isset($value)) {echo '<span class="note">NULL - not set</span>';}
				elseif(empty($value)) {echo '<span class="note">string(0) - set, but empty</span>';}
				else {echo $value;};
			?>
			</code><br />
		<?php } else : echo 'No options data!'; endif; ?>
		</div>
	</div>

	<div class="row row-setting row-omega">
		<div class="col-sm-3 col-name">print_r</div>
		<div class="col-sm-7 col-setting">
			<pre><?php print_r($options); ?></pre>
		</div>
	</div>

<?php }

 ?>