# Muffin Boilerplate Theme

Boilerplate theme from my time freelancing. Originally built around 2015.

It uses Bootstrap and custom SASS for the frontend.

The backend is all custom. It adds custom widgets with a builder class, and colourful custom option pages.
